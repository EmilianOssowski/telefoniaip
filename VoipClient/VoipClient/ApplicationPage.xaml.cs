﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VoipClient
{
    /// <summary>
    /// Interaction logic for ApplicationPage.xaml
    /// </summary>
    public partial class ApplicationPage : Page
    {
        
            public User user;
            public MainWindow mw;
            SqlConnection m_dbConnection = new SqlConnection("Server=tcp:osadb.database.windows.net,1433;Initial Catalog=tip;Persist Security Info=False;User ID=benek;Password=Paradise6850;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            List<int> contactsIdArray = new List<int>();
            List<User> contactsArray = new List<User>();
           


            public ApplicationPage(User user, MainWindow mw)
            {
                this.mw = mw;
                InitializeComponent();
                this.user = user;
                frame.Visibility = System.Windows.Visibility.Hidden;
            InitializeComponent();

            }

            private void BTlogoff_Click(object sender, RoutedEventArgs e)
            {
                this.user.finalize();
                MainWindow logout = new MainWindow();
                mw.Close();
                logout.Show();

            }

            private void BTcontacts_Click(object sender, RoutedEventArgs e)
            {

            try
            {
                m_dbConnection.Open();
                string sql = "SELECT idUser FROM TelIP_Match m WHERE m.idContactList = @idContactList";
                SqlCommand command = new SqlCommand(sql, m_dbConnection);
                command.Parameters.Add(new SqlParameter("@idContactList", user.IdContactList));

                
                DataTable dt = new DataTable();
                SqlDataReader queryResult = command.ExecuteReader();
                dt.Load(queryResult);



                contactsIdArray.Clear();
                

                foreach (DataRow row in dt.Rows)
                {
                    contactsIdArray.Add(Convert.ToInt32(row[0].ToString()));

                }

                contactsArray.Clear(); 
                foreach (int i in contactsIdArray)
                {

                    string sqlUser = "SELECT u.username, u.mail, u.adress, u.status, c.idContactList FROM TelIP_User u JOIN TelIP_ContactList c ON c.idOwner=u.idUser WHERE u.idUser = @idUser AND c.idOwner = @idUser";
                    SqlCommand commandUser = new SqlCommand(sqlUser, m_dbConnection);
                    commandUser.Parameters.Add(new SqlParameter("@idUser", i));

                    DataTable dtUser = new DataTable();
                    SqlDataReader queryResultUser = commandUser.ExecuteReader();
                    dtUser.Load(queryResultUser);
                    DataRow dw = dtUser.Rows[0];

                    User u = new User();

                    u.Username = dw[0].ToString();
                    u.Mail = dw[1].ToString();
                    u.Adress = dw[2].ToString();
                    u.Status = Convert.ToBoolean(dw[3].ToString());
                    u.IdContactList = int.Parse(dw[4].ToString());
                 
                    u.IdUser = i;
                    contactsArray.Add(u);



                }


               
            }
            catch (SqlException)
            {
                //pusta lista
            }
            m_dbConnection.Close();
            Contacts contacts = new Contacts(frame, BTcontacts, contactsArray, user, mainFrame);
                frame.Visibility = System.Windows.Visibility.Visible;
                frame.Navigate(contacts);
                BTcontacts.Visibility = System.Windows.Visibility.Hidden;


            }
            public Call Navigate(string who)
            {
            try
            {
                m_dbConnection.Open();
                string sql = "SELECT idUser FROM TelIP_Match m WHERE m.idContactList = @idContactList";
                SqlCommand command = new SqlCommand(sql, m_dbConnection);
                command.Parameters.Add(new SqlParameter("@idContactList", user.IdContactList));


                DataTable dt = new DataTable();
                SqlDataReader queryResult = command.ExecuteReader();
                dt.Load(queryResult);



                contactsIdArray.Clear();


                foreach (DataRow row in dt.Rows)
                {
                    contactsIdArray.Add(Convert.ToInt32(row[0].ToString()));

                }

                contactsArray.Clear();
                foreach (int i in contactsIdArray)
                {

                    string sqlUser = "SELECT u.username, u.mail, u.adress, u.status, c.idContactList FROM TelIP_User u JOIN TelIP_ContactList c ON c.idOwner=u.idUser WHERE u.idUser = @idUser AND c.idOwner = @idUser";
                    SqlCommand commandUser = new SqlCommand(sqlUser, m_dbConnection);
                    commandUser.Parameters.Add(new SqlParameter("@idUser", i));

                    DataTable dtUser = new DataTable();
                    SqlDataReader queryResultUser = commandUser.ExecuteReader();
                    dtUser.Load(queryResultUser);
                    DataRow dw = dtUser.Rows[0];

                    User u = new User();

                    u.Username = dw[0].ToString();
                    u.Mail = dw[1].ToString();
                    u.Adress = dw[2].ToString();
                    u.Status = Convert.ToBoolean(dw[3].ToString());
                    u.IdContactList = int.Parse(dw[4].ToString());

                    u.IdUser = i;
                    contactsArray.Add(u);



                }



            }
            catch (SqlException)
            {
                //pusta lista
            }
            m_dbConnection.Close();
            User callee = contactsArray.Find(item => item.Username == who);
                Call c = new VoipClient.Call(callee, mainFrame, user);
                mainFrame.Navigate(c);
            return c;
            }
        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
            {
                frame.Navigate(null);
                frame.Visibility = System.Windows.Visibility.Hidden;
                BTcontacts.Visibility = System.Windows.Visibility.Visible;
            }
        }
    }
