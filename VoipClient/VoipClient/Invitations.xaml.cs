﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VoipClient
{
    /// <summary>
    /// Interaction logic for Invitations.xaml
    /// </summary>
    public partial class Invitations : Page
    {
        User mainUser;
        Frame frame;
        List<User> invitationsArray = new List<User>();
        List<int> invitationsIdArray = new List<int>();
        List<User> contactsArray ;
        Frame mainFrame;
        Button bt;

        SqlConnection m_dbConnection = new SqlConnection("Server=tcp:osadb.database.windows.net,1433;Initial Catalog=tip;Persist Security Info=False;User ID=benek;Password=Paradise6850;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

        private void BTcontactsExit_Click(object sender, RoutedEventArgs e)
        {
            Contacts contacts = new Contacts(frame, bt, contactsArray, mainUser, mainFrame);
            frame.Navigate(contacts);
        }
        public Invitations(User mainUser, Frame frame, Frame mainFrame , List<User> contactsArray, Button bt)
        {
            InitializeComponent();
            this.mainUser = mainUser;
            this.frame = frame;
            this.mainFrame = mainFrame;
            this.bt = bt;
            this.contactsArray = contactsArray;
            try
            {
                m_dbConnection.Open();

                string sql = "SELECT idHost FROM TelIP_Invitations i WHERE i.idGuest = @idMainUser";
                SqlCommand command = new SqlCommand(sql, m_dbConnection);
                command.Parameters.Add(new SqlParameter("@idMainUser", mainUser.IdUser));


                DataTable dt = new DataTable();
                SqlDataReader queryResult = command.ExecuteReader();
                dt.Load(queryResult);



                invitationsIdArray.Clear();


                foreach (DataRow row in dt.Rows)
                {
                    invitationsIdArray.Add(Convert.ToInt32(row[0].ToString()));

                }

                invitationsArray.Clear();
                foreach (int i in invitationsIdArray)
                {

                    string sqlUser = "SELECT u.username, u.mail, u.adress, u.status, c.idContactList FROM TelIP_User u JOIN TelIP_ContactList c ON c.idOwner=u.idUser WHERE u.idUser = @idUser AND c.idOwner = @idUser";
                    SqlCommand commandUser = new SqlCommand(sqlUser, m_dbConnection);
                    commandUser.Parameters.Add(new SqlParameter("@idUser", i));

                    DataTable dtUser = new DataTable();
                    SqlDataReader queryResultUser = commandUser.ExecuteReader();
                    dtUser.Load(queryResultUser);
                    DataRow dw = dtUser.Rows[0];

                    User u = new User();

                    u.Username = dw[0].ToString();
                    u.Mail = dw[1].ToString();
                    u.Adress = dw[2].ToString();
                    u.Status = Convert.ToBoolean(dw[3].ToString());
                    u.IdContactList = int.Parse(dw[4].ToString());

                    u.IdUser = i;
                    invitationsArray.Add(u);
                    m_dbConnection.Close();
                }
            }
            catch (SqlException)
            {
                //pusta lista
            }
           // invlistBox.ItemsSource = invitationsArray;
           foreach (User u in invitationsArray)
           {
               invlistBox.Items.Add(u);
           }
            

        }


        private void invlistBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            User u = (User)invlistBox.SelectedItem;
            if (MessageBox.Show("Dodać użytkownika do listy znajomych?", "Nowa znajomość", MessageBoxButton.YesNo) == MessageBoxResult.No)
            {
                m_dbConnection.Open();

                string sqlDeleteInvitation = "DELETE FROM TelIP_Invitations WHERE idHost = @idHost  AND idGuest = @idGuest";
                SqlCommand commandDeleteInvitation = new SqlCommand(sqlDeleteInvitation, m_dbConnection);
                commandDeleteInvitation.Parameters.Add(new SqlParameter("@idHost", u.IdUser));
                commandDeleteInvitation.Parameters.Add(new SqlParameter("@idGuest", mainUser.IdUser));

                commandDeleteInvitation.ExecuteNonQuery();

                m_dbConnection.Close();
            }
            else
            {
                //do yes stuff
                ///Dodawanie znajomości do bazy
                m_dbConnection.Open();

                string sqlAddContact = "INSERT INTO TelIP_Match(idContactList, idUser) values(@idContactList, @idUser)";
                SqlCommand commandAddContact = new SqlCommand(sqlAddContact, m_dbConnection);
                commandAddContact.Parameters.Add(new SqlParameter("@idContactList", mainUser.IdContactList));
                commandAddContact.Parameters.Add(new SqlParameter("@idUser", u.IdUser));

                commandAddContact.ExecuteNonQuery();

                //Dodawanie kontrznajomości
                string sqlAddContrContact = "INSERT INTO TelIP_Match(idContactList, idUser) values(@idContactList, @idUser)";
                SqlCommand commandAddContrContact = new SqlCommand(sqlAddContrContact, m_dbConnection);
                commandAddContrContact.Parameters.Add(new SqlParameter("@idContactList", u.IdContactList));
                commandAddContrContact.Parameters.Add(new SqlParameter("@idUser", mainUser.IdUser));

                commandAddContrContact.ExecuteNonQuery();

                string sqlDeleteInvitation = "DELETE FROM TelIP_Invitations WHERE idHost = @idHost  AND idGuest = @idGuest";
                SqlCommand commandDeleteInvitation = new SqlCommand(sqlDeleteInvitation, m_dbConnection);
                commandDeleteInvitation.Parameters.Add(new SqlParameter("@idHost", u.IdUser));
                commandDeleteInvitation.Parameters.Add(new SqlParameter("@idGuest", mainUser.IdUser));

                commandDeleteInvitation.ExecuteNonQuery();

                m_dbConnection.Close();
            }
        }
    }
}
