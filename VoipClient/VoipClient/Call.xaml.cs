﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using NAudio.CoreAudioApi;
using FragLabs.Audio.Codecs;
using NAudio.Wave;
using TIPimpl;


namespace VoipClient
{
    /// <summary>
    /// Interaction logic for Call.xaml
    /// </summary>
    public partial class Call : Page
    {
        User user;
        public User mainUser;
        Frame mainFrame;
        //Voice
        static public int volumein = 0;
        static public int volumeout = 0;
        bool callinprog = false;
        DispatcherTimer _dispatcherTimer = null;
        static public int lastmax = 100;
        private void Dt_Tick(object sender, object e)
        {
            if (MainWindow.voicehandler != null)
            {
                volumein = VoiceHandling.volume_in;
                volumeout = VoiceHandling.volume_out;

                if (volumein > lastmax)
                {
                    lastmax = volumein;
                }
                MainWindow.voicehandler.lastmax = lastmax;
                INPROG.Maximum = lastmax;
                INPROG.Value = volumein;
                OUTPROG.Maximum = lastmax;
                OUTPROG.Value = volumeout;
            }
        }
        public List<String> populateIN_devices()
        {
            List<String> iNList = new List<String>();
            for (int i = 0; i < WaveIn.DeviceCount; i++)
            {
                iNList.Add(WaveIn.GetCapabilities(i).ProductName);
            }
            return iNList;
        }
        public List<String> populateOUT_devices()
        {
            List<String> oUTList = new List<String>();
            for (int i = 0; i < WaveOut.DeviceCount; i++)
            {
                oUTList.Add(WaveOut.GetCapabilities(i).ProductName);
            }
            return oUTList;
        }
        //Voice


        public Call( User user, Frame mainFrame, User mainUser)
        {
            InitializeComponent();
            this.mainUser = mainUser;
            this.user = user;
            this.mainFrame = mainFrame;
            LBusername.Content = user.Username;

            //Voice
            _dispatcherTimer = new DispatcherTimer();
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            _dispatcherTimer.Tick += Dt_Tick;
            _dispatcherTimer.Start();
            //SplashScreen splash = new SplashScreen("Resources/su35b.gif");
            //splash.Show(true);
            //voicehandler = new VoiceHandling();
            iNList.ItemsSource = populateIN_devices();
            if (WaveIn.DeviceCount > 0)
                iNList.SelectedIndex = 0;


            oUTList.ItemsSource = populateOUT_devices();
            if (WaveOut.DeviceCount > 0)
                oUTList.SelectedIndex = 0;
            //Voice
        }

        private void BTexitCall_Click(object sender, RoutedEventArgs e)
        {
            //Voice

            MainWindow.voicehandler.Stop_it();
                volumein = 0;
                volumeout = 0;
                callinprog = false;
            //MainWindow.voicehandler = null;
            //MainWindow.voicehandler = new VoiceHandling();

            //Voice
            OpenContact oc = new OpenContact(user, mainFrame, mainUser);
            mainFrame.Navigate(oc);
            Negotiation.EndCall(mainUser.Username, user.Adress);
            
        }

        public void BTstartCall_Click(object sender, RoutedEventArgs e)
        {

            //Voice
            if (!callinprog)
            {
                //voicehandler = new VoiceHandling();
                MainWindow.voicehandler.callme(iNList.SelectedIndex, oUTList.SelectedIndex, user.Adress, mainUser.Username); //TO_DO Add the name of the user calling (osoby ktora dzwoni)
                //voicehandler.callme(iNList.SelectedIndex, oUTList.SelectedIndex, "127.0.0.1mainUser
                callinprog = true;
            }
            //Voice
        }

        public void JoinCall()
        {

            //Voice
            if (!callinprog)
            {
                //voicehandler = new VoiceHandling();
                MainWindow.voicehandler.callme(iNList.SelectedIndex, oUTList.SelectedIndex, user.Adress, mainUser.Username); //TO_DO Add the name of the user calling (osoby ktora dzwoni)
                //voicehandler.callme(iNList.SelectedIndex, oUTList.SelectedIndex, "127.0.0.1");
                callinprog = true;
            }
            //Voice
        }

    }
}
