﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using FragLabs.Audio.Codecs;
using System.Threading;
using System.Diagnostics;
using VoipClient;
using System.Windows;

namespace TIPimpl
{
    public class VoiceHandling
    {
        public WaveIn waveIn = null;
        public WaveOut waveOut = null;
        public static BufferedWaveProvider playBuffer = null;
        OpusEncoder encoder = null;
        static OpusDecoder decoder = null;
        int segmentFrames = 0;
        int bytesPerSegment = 0;
        ulong bytesSent = 0;
        DateTime startTime = DateTime.Now;
        Networking network = null;
        int sum = 0;
        static int outsum = 0;
        static public int volume_in = 0;
        static public int volume_out = 0;
        static public int incomingpackets = 0;
        public int lastmax = 0;
        static public bool accepted = false;
        byte[] notEncodedBuffer = new byte[0];
        byte[] EncodedBuffer = new byte[0];
        byte[] soundBuffer = new byte[0];
        public VoiceHandling()
        {
            Negotiation.initializefunc();
            //this.network = new Networking();
        }
        
        
        public void callme(int device_num_in, int device_num_out, string ip, string myname)
        {
            waveIn = new WaveIn();
            waveOut = new WaveOut();
            network = new Networking(ip);
            if (VoiceHandling.accepted)
            {
                Negotiation.callinprog = true;
                Negotiation.AcceptCall(myname, ip);
            }
            else
            {
                Negotiation.StartCall(myname,ip);
                VoiceHandling.accepted = true;
            }
            Record_OPUS(device_num_in);
            Play_OPUS(device_num_out);
        }


        public void Record_OPUS(int device_num)
        {
            startTime = DateTime.Now;
            bytesSent = 0;
            segmentFrames = 960;
            encoder = OpusEncoder.Create(48000, 1, FragLabs.Audio.Codecs.Opus.Application.Voip);
            encoder.Bitrate = 8192;
            bytesPerSegment = encoder.FrameByteCount(segmentFrames);

            waveIn = new WaveIn(WaveCallbackInfo.FunctionCallback());
            waveIn.BufferMilliseconds = 50;
            waveIn.DeviceNumber = device_num;
            waveIn.DataAvailable += waveIn_DataAvailableEvent;
            waveIn.WaveFormat = new WaveFormat(48000, 16, 1);
            waveIn.StartRecording();
        }


        public void Play_OPUS(int device_num)
        {
            decoder = OpusDecoder.Create(48000, 1);
            network.Datalisten();
            playBuffer = new BufferedWaveProvider(new WaveFormat(48000, 16, 1));
            waveOut = new WaveOut(WaveCallbackInfo.FunctionCallback());
            waveOut.DeviceNumber = device_num;
            waveOut.Init(playBuffer);
            waveOut.Play();
        }
        public void waveIn_DataAvailableEvent(object sender, WaveInEventArgs e)
        {
            sum = 0;
            for (int i = 0; i < 8; i++)
            {
                sum += LossyAbs(BitConverter.ToInt16(e.Buffer, 200 * i));
            }
            sum /= 8;
            volume_in = sum;
            //Debug.WriteLine(Negotiation.callinprog);
            if (sum > lastmax * 0.04 && Negotiation.callinprog)
            {
                soundBuffer = new byte[e.BytesRecorded + notEncodedBuffer.Length]; //Legnht = new data + old data
                for (int i = 0; i < notEncodedBuffer.Length; i++)   //First we try encode as much as we can from old data
                    soundBuffer[i] = notEncodedBuffer[i];
                for (int i = 0; i < e.BytesRecorded; i++)
                    soundBuffer[i + notEncodedBuffer.Length] = e.Buffer[i];
            }
            else
            {
                soundBuffer = new byte[notEncodedBuffer.Length];
                for (int i = 0; i < notEncodedBuffer.Length; i++)   //First we try encode as much as we can from old data
                    soundBuffer[i] = notEncodedBuffer[i];
            }

            if (soundBuffer.Length != 0)
            {
                int byteCap = bytesPerSegment;
                int segmentCount = (int)Math.Floor((decimal)soundBuffer.Length / byteCap);
                int segmentsEnd = segmentCount * byteCap;
                int notEncodedCount = soundBuffer.Length - segmentsEnd;
                notEncodedBuffer = new byte[notEncodedCount];
                for (int i = 0; i < notEncodedCount; i++)
                {
                    notEncodedBuffer[i] = soundBuffer[segmentsEnd + i];
                }
                int len;

                for (int i = 0; i < segmentCount; i++)
                {
                    byte[] segment = new byte[byteCap];
                    for (int j = 0; j < segment.Length; j++)
                        segment[j] = soundBuffer[(i * byteCap) + j];
                    byte[] EncodedBuffer = encoder.Encode(segment, segment.Length, out len);
                    bytesSent += (ulong)len;
                    byte[] CutEncoded = new byte[len];
                    Array.Copy(EncodedBuffer, CutEncoded, len);
                    //send
                    //decode(CutEncoded, CutEncoded.Length);
                    network.Datasend(CutEncoded);
                }
            }
        }

        public static short LossyAbs(short value)
        {
            if (value >= 0) return value;
            if (value == short.MinValue) return short.MaxValue;
            return Math.Abs(value);
        }

        static public void decode(byte[] buff, int len)
        {
            if (VoiceHandling.accepted)
            {
                buff = decoder.Decode(buff, len, out len);
                outsum = 0;
                for (int i = 0; i < 8; i++)
                {
                    outsum += LossyAbs(BitConverter.ToInt16(buff, 200 * i));
                }
                outsum /= 8;
                volume_out = outsum;
                playBuffer.AddSamples(buff, 0, len);
                incomingpackets++;
            }
        }

        static public void Calling(string who)
        {
            //TO_DO show who is calling, go to call screen if accepted do all the stuff necessary to start the call as if you werer the one calling
            //System.Windows.MessageBox.Show("dzwoni " + who);
            string sMessageBoxText = "Dzwoni " + who;
            string sCaption = "Połączenie";

            MessageBoxButton btnMessageBox = MessageBoxButton.YesNo;
            MessageBoxImage icnMessageBox = MessageBoxImage.Warning;

            MessageBoxResult rsltMessageBox = MessageBox.Show(sMessageBoxText, sCaption, btnMessageBox, icnMessageBox);

            switch (rsltMessageBox)
            {
                case MessageBoxResult.Yes:
                    {
                      VoiceHandling.accepted = true;
                        // ApplicationPage apppage = (ApplicationPage)MainWindow.mw.Content;
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            //Debug.WriteLine(who);
                            MainWindow.pleasework(who);
                            
                        }));
                        
                       // ((ApplicationPage)MainWindow.mw.Content).Navigate(who).JoinCall();
                        break;
                    }


                case MessageBoxResult.No:
                    {
                        VoiceHandling.accepted = false;
                        //Negotiation.NoCall("",);
                        break;
                    }
            }

        }
        static public void NoCall(string who)
        {
            System.Windows.MessageBox.Show("Połączenie odrzucone " + who);
            VoiceHandling.accepted = false;

        }
        static public void EndCall(string who)
        {
            System.Windows.MessageBox.Show("Połączenie zakończone z " + who);
            VoiceHandling.accepted = false;

        }

        public void Stop_it()
        {
            if (waveIn != null)
            {
                waveIn.StopRecording();
                waveIn.Dispose();
                waveIn = null;
            }
            if (network != null)
            {
                network.Closeconn();
                network = null;
            }

            if (waveOut != null)
            {
                waveOut.Stop();
                waveOut.Dispose();
                waveOut = null;
            }
            notEncodedBuffer = new byte[0];
            EncodedBuffer = new byte[0];
            soundBuffer = new byte[0];
            playBuffer = null;
            if (encoder != null)
            {
                encoder.Dispose();
                encoder = null;
            }
            if (decoder != null)
            {
                decoder.Dispose();
                decoder = null;
            }
            VoiceHandling.accepted = false;


        }
        public void Stop_recording()
        {
            network.Closeconn();
            waveIn.StopRecording();
            waveIn.Dispose();
            waveIn = null;
            if (encoder != null)
            {
                encoder.Dispose();
                encoder = null;
            }           
        }
        public void Stop_playing()
        {
            network.Closeconn();            
            waveOut.Stop();
            waveOut.Dispose();
            waveOut = null;
            playBuffer = null;
            if (decoder != null)
            {
                decoder.Dispose();
                decoder = null;
            }

        }


    }
}
