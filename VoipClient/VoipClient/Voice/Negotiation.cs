﻿using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Connections.UDP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TIPimpl;

namespace TIPimpl
{
    public static class Negotiation
    {
        static bool initialised = false;
        static public bool callinprog = false;
        static public bool callerfive = false;
        public static List<ConnectionListenerBase> listeners = null;
        public static void initializefunc()
        {
            if (Negotiation.initialised) return;
            else
            {
                Connection.StopListening();
                //connInfo = new ConnectionInfo(ip, 10000);
                //newUDPConn = UDPConnection.GetConnection(connInfo, UDPOptions.None);
                NetworkComms.AppendGlobalIncomingPacketHandler<string>("callstarting", Youarebeingcalled);
                NetworkComms.AppendGlobalIncomingPacketHandler<string>("callaccepted", Callaccepted);
                NetworkComms.AppendGlobalIncomingPacketHandler<string>("nocall", Nocall);
                NetworkComms.AppendGlobalIncomingPacketHandler<string>("endcall", Endcall);
                Negotiation.listeners = Connection.StartListening(ConnectionType.UDP, new IPEndPoint(IPAddress.Any, 12402));

                Negotiation.initialised = true;
            }
        }

        static public void StartCall(string yourname, string ip)
        {
            UDPConnection.SendObject("callstarting", yourname, new IPEndPoint(IPAddress.Parse(ip), 12402));
            Negotiation.callerfive = true;
        }
        static public void AcceptCall(string yourname, string ip)
        {
            UDPConnection.SendObject("callaccepted", yourname, new IPEndPoint(IPAddress.Parse(ip), 12402));
            Negotiation.callinprog = true;
            Negotiation.callerfive = false;
        }
        static public void NoCall(string yourname, string ip)
        {
            UDPConnection.SendObject("nocall", yourname, new IPEndPoint(IPAddress.Parse(ip), 12402));
            Negotiation.callinprog = false;

        }

        static public void EndCall(string yourname, string ip)
        {
            UDPConnection.SendObject("endcall", yourname, new IPEndPoint(IPAddress.Parse(ip), 12402));
            Negotiation.callerfive = false;
            Negotiation.callinprog = false;
        }
        static public void Closeconn()
        {
            //newUDPConn.CloseConnection(false);
            Connection.StopListening(Negotiation.listeners);
            Connection.StopListening();
            NetworkComms.CloseAllConnections();
            NetworkComms.RemoveGlobalIncomingPacketHandler<string>("callstarting", Youarebeingcalled);
            NetworkComms.RemoveGlobalIncomingPacketHandler<string>("callaccepted", Callaccepted);
            NetworkComms.RemoveGlobalIncomingPacketHandler<string>("nocall", Nocall);
            NetworkComms.RemoveGlobalIncomingPacketHandler<string>("endcall", Endcall);
            NetworkComms.RemoveGlobalIncomingPacketHandler();
            NetworkComms.Shutdown();
            Debug.WriteLine("shuttingdow");

        }

        static private void Youarebeingcalled(PacketHeader packetHeader, Connection connection, string incomingObject)
        {
            Debug.WriteLine(connection.ConnectionInfo.RemoteEndPoint.ToString());
            VoiceHandling.Calling(incomingObject);
            Negotiation.callerfive = false;
        }
        static private void Callaccepted(PacketHeader packetHeader, Connection connection, string incomingObject)
        {
            Negotiation.callinprog = true;
            Negotiation.callerfive = true;
            Debug.WriteLine("DRUGA STRONA AKCEPTUJE");
        }
        static private void Nocall(PacketHeader packetHeader, Connection connection, string incomingObject)
        {
            VoiceHandling.NoCall(incomingObject);
            Negotiation.callinprog = false;
        }
        static private void Endcall(PacketHeader packetHeader, Connection connection, string incomingObject)
        {
            VoiceHandling.EndCall(incomingObject);
            Negotiation.callinprog = false;
        }
    }
}

