﻿using NetworkCommsDotNet;
using NetworkCommsDotNet.Connections;
using NetworkCommsDotNet.Connections.UDP;
using NetworkCommsDotNet.DPSBase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TIPimpl
{
    class Networking
    {
        List<ConnectionListenerBase> listener = null;
        string ip = "";
        public Networking(string ip)
        {
            this.ip = ip;
        }

        public void Closeconn()
        {
            //newUDPConn.CloseConnection(false);
            //NetworkComms.CloseAllConnections();
            NetworkComms.RemoveGlobalIncomingPacketHandler<byte[]>("icecream", Unamangedbytevoice);
            //Connection.StopListening(ConnectionType.UDP);
            Connection.StopListening(this.listener);
  //          NetworkComms.RemoveGlobalIncomingPacketHandler();
  //          Debug.WriteLine(NetworkComms.GlobalIncomingPacketHandlerExists("icecream"));

            //           NetworkComms.Shutdown();

        }

        public void Datasend(byte[] buff)
        {
            // NetworkComms.SendObject("VOICE", buff, newUDPConn);
            //newUDPConn.SendObject<byte[]>("icecream", buff);
            //UDPConnection.SendObject<byte[]>("icecream", buff, new IPEndPoint(IPAddress.Parse("127.0.0.1"), 10000));
            if (Negotiation.callerfive)
            {
                UDPConnection.SendObject("icecream", buff, new IPEndPoint(IPAddress.Parse(ip), 10005));
            }
            else
            {
                UDPConnection.SendObject("icecream", buff, new IPEndPoint(IPAddress.Parse(ip), 10000));
            }
            //Debug.WriteLine(ip);

        }
        public void Datalisten()
        {
            if (Negotiation.callerfive)
            {
                this.listener = Connection.StartListening(ConnectionType.UDP, new IPEndPoint(IPAddress.Any, 10000));
            }
            else
            {
                this.listener = Connection.StartListening(ConnectionType.UDP, new IPEndPoint(IPAddress.Any, 10005));
            }
            NetworkComms.AppendGlobalIncomingPacketHandler<byte[]>("icecream", Unamangedbytevoice);
        }
        private void Unamangedbytevoice(PacketHeader packetHeader, Connection connection, byte[] incomingObject)
        {
            VoiceHandling.decode(incomingObject, incomingObject.Length);
        }

    }
}
