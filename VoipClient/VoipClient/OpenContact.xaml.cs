﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VoipClient
{
    /// <summary>
    /// Interaction logic for OpenContact.xaml
    /// </summary>
    public partial class OpenContact : Page
    {
        User mainUser;
        User user;
        Frame mainFrame;
        public OpenContact(User user, Frame mainFrame, User mainUser)
        {
            this.mainUser = mainUser;
            this.user = user;
            this.mainFrame = mainFrame;
            InitializeComponent();
            LBusername.Content = user.Username;
            LBmail.Content = user.Mail;
        }

        private void BTcall_Click(object sender, RoutedEventArgs e)
        {
            Call c = new VoipClient.Call(user, mainFrame, mainUser);
            mainFrame.Navigate(c);
        }
    }
}
