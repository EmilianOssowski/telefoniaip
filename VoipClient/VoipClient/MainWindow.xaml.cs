﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using TIPimpl;

namespace VoipClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public User user;
        static public VoiceHandling voicehandler = new VoiceHandling();
        static public MainWindow mw;
        static public ApplicationPage magic;
        public MainWindow()
        {
            InitializeComponent();
            //voicehandler = new VoiceHandling();
        }

        //SqlConnection m_dbConnection = new SqlConnection("Data Source=C:\\Users\\Osa\\Source\\Repos\\TelefoniaIP\\VoipClient\\VoipClient\\TIP.s3db");
        SqlConnection m_dbConnection = new SqlConnection("Server=tcp:osadb.database.windows.net,1433;Initial Catalog=tip;Persist Security Info=False;User ID=benek;Password=Paradise6850;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

        public static string HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();

        }



        public bool emailConfirm(string s)
        {
            Regex regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            return regex.IsMatch(s);
        }


        public static string SHA256Hash(string s)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            var sha256 = SHA256.Create();
            byte[] hashBytes = sha256.ComputeHash(bytes);
            return HexStringFromBytes(hashBytes);

        }
        private bool passwordConfirm(string s)
        {
            if (s.Length < 8) return false;
            else return true;
        }
        private void BTregister_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                if (TBusernameReg.Text == "" || PBpasswordReg.Password == "" || PBpasswordConfirmationReg.Password == "" || TBmail.Text == "" || PBpasswordReg.Password != PBpasswordConfirmationReg.Password) throw new Exception("Puste pola");
                if (emailConfirm(TBmail.Text) == false) throw new Exception("błędny adres e-mail");
                if(passwordConfirm(PBpasswordReg.Password.ToString()) == false) throw new Exception("za krótkie hasło");
                m_dbConnection.Open();
                string sql = "INSERT INTO TelIP_User(username, password, mail) VALUES (@username,@password,@mail)";
                SqlCommand command = new SqlCommand(sql, m_dbConnection);
                command.Parameters.Add(new SqlParameter("@username", TBusernameReg.Text));
                command.Parameters.Add(new SqlParameter("@password", SHA256Hash(PBpasswordReg.Password.ToString())));
                command.Parameters.Add(new SqlParameter("@mail", TBmail.Text));
                command.ExecuteNonQuery();


                string sqlContacts = "INSERT INTO TelIP_ContactList(idOwner) SELECT idUser FROM TelIP_User WHERE username = @username";
                SqlCommand commandContacts = new SqlCommand(sqlContacts, m_dbConnection);
                commandContacts.Parameters.Add(new SqlParameter("@username", TBusernameReg.Text));
                commandContacts.ExecuteNonQuery();

                m_dbConnection.Close();
                TBusernameReg.Text = "";
                PBpasswordReg.Password = "";
                PBpasswordConfirmationReg.Password = "";
                TBmail.Text = "";
                MessageBox.Show("Rejestracja udana", "Sukces");

            }


            catch (System.IndexOutOfRangeException)
            {
                MessageBox.Show("Podaj login, hasło i e-mail", "Blad");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Blad");
                m_dbConnection.Close();
            }
            m_dbConnection.Close();
        }

        private void BTlogin_Click(object sender, RoutedEventArgs e)
        {


            try
            {
                m_dbConnection.Open();
                string sql = "SELECT username, password, idUser, idContactList FROM TelIP_User u JOIN TelIP_ContactList c ON c.idOwner=u.idUser WHERE username = @username AND password = @password";
                SqlCommand command = new SqlCommand(sql, m_dbConnection);
                command.Parameters.Add(new SqlParameter("@username", TBusername.Text));
                command.Parameters.Add(new SqlParameter("@password", SHA256Hash(TBpassword.Password.ToString())));



                DataTable dt = new DataTable();
                SqlDataReader queryResult = command.ExecuteReader();
                dt.Load(queryResult);
                DataRow dw = dt.Rows[0];

                User u = new User();

                u.Username = dw[0].ToString();
                u.Password = dw[1].ToString();
                u.IdUser = int.Parse(dw[2].ToString());
                u.IdContactList = int.Parse(dw[3].ToString());
                u.IsMainUser = true;

                if (u.Username == TBusername.Text && u.Password == SHA256Hash(TBpassword.Password.ToString()))
                {
                   // string pubIp = new System.Net.WebClient().DownloadString("https://api.ipify.org");
                    //string sqlStatus = "UPDATE TelIP_User SET status = 'true' WHERE idUser = @idUser";
                    string sqlStatus = "UPDATE TelIP_User SET status = 'true', adress = @ipAdress WHERE idUser = @idUser";
                    SqlCommand statusCommand = new SqlCommand(sqlStatus, m_dbConnection);
                    statusCommand.Parameters.Add(new SqlParameter("@idUser", u.IdUser));
                    statusCommand.Parameters.Add(new SqlParameter("@ipAdress", getLocalIP()));
                    statusCommand.ExecuteNonQuery();

                    mw = new MainWindow();
                    magic = new ApplicationPage(u, mw);
                    mw.Content = magic;
                    this.Close();
                    mw.Show();

                    // Height="438" Width="462"
                    mw.Height = 635;
                    mw.Width = 815;


                }
                else
                {

                    MessageBox.Show("Złe dane logowania", "BŁĄD");


                }
                m_dbConnection.Close();

            }
            catch (System.IndexOutOfRangeException)
            {
                MessageBox.Show("Podaj login i hasło", "Blad");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Blad");
            }

            m_dbConnection.Close();

        }
        private string getLocalIP()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if(ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }
        public static void pleasework(string who)
        {
            ((ApplicationPage)MainWindow.mw.Content).Navigate(who).JoinCall();
        }
        private void TBmail_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            bool result = emailConfirm(TBmail.Text);

        }


    }
}
