﻿using System;
using System.Collections.Generic;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace VoipClient
{

    public class User
    {
        private int idUser;
        private string username;
        private string password;
        private string mail;
        private string adress = null;
        private bool status;
        private int idContactList;
        private bool isMainUser = false;
        

        public int IdUser
        {
            get
            {
                return idUser;
            }

            set
            {
                idUser = value;
            }
        }

        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public string Mail
        {
            get
            {
                return mail;
            }

            set
            {
                mail = value;
            }
        }

        public string Adress
        {
            get
            {
                return adress;
            }

            set
            {
                adress = value;
            }
        }

        public bool Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public int IdContactList
        {
            get
            {
                return idContactList;
            }

            set
            {
                idContactList = value;
            }
        }

        public bool IsMainUser
        {
            get
            {
                return isMainUser;
            }

            set
            {
                isMainUser = value;
            }
        }
        ~User()
        {
            if(this.isMainUser == true)
            {
                try
                {
                    SqlConnection m_dbConnection = new SqlConnection("Server=tcp:osadb.database.windows.net,1433;Initial Catalog=tip;Persist Security Info=False;User ID=benek;Password=Paradise6850;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
                    m_dbConnection.Open();
                    string sqlStatus = "UPDATE TelIP_User SET status = 'false' WHERE idUser = @idUser";
                    SqlCommand statusCommand = new SqlCommand(sqlStatus, m_dbConnection);
                    statusCommand.Parameters.Add(new SqlParameter("@idUser", this.IdUser));
                    statusCommand.ExecuteNonQuery();
                    m_dbConnection.Close();
                }
                catch (Exception)
                {

                }
            }
        }


        public void finalize()
        {
            SqlConnection m_dbConnection = new SqlConnection("Server=tcp:osadb.database.windows.net,1433;Initial Catalog=tip;Persist Security Info=False;User ID=benek;Password=Paradise6850;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            m_dbConnection.Open();
            string sqlStatus = "UPDATE TelIP_User SET status = 'false' WHERE idUser = @idUser";
            SqlCommand statusCommand = new SqlCommand(sqlStatus, m_dbConnection);
            statusCommand.Parameters.Add(new SqlParameter("@idUser", this.IdUser));
            statusCommand.ExecuteNonQuery();
            m_dbConnection.Close();
        }
    }

}
