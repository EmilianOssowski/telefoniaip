﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VoipClient
{
    /// <summary>
    /// Interaction logic for Contacts.xaml
    /// </summary>
    public partial class Contacts : Page
    {
        User user;
        Frame frame;
        Button bt;
        List<User> contactsArray = new List<User>();
        Frame mainFrame;
        public Contacts(Frame frame, Button bt, List<User> contactsArray, User user, Frame mainFrame)
        {
            InitializeComponent();
            this.frame = frame;
            this.bt = bt;
            this.contactsArray = contactsArray;
            this.user = user;
            this.mainFrame = mainFrame;
            foreach (User u in contactsArray)
            {
                listBox.Items.Add(u);
            }


        }

        private void BTcontactsExit_Click(object sender, RoutedEventArgs e)
        {
            frame.Navigate(null);
            frame.Visibility = System.Windows.Visibility.Hidden;
            bt.Visibility = System.Windows.Visibility.Visible;

        }



        private void listBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            User u = (User)listBox.SelectedItem;
            OpenContact oc = new OpenContact(u, mainFrame, user);
            mainFrame.Navigate(oc);

            frame.Navigate(null);
            frame.Visibility = System.Windows.Visibility.Hidden;
            bt.Visibility = System.Windows.Visibility.Visible;
        }

        private void BTaddContact_Click(object sender, RoutedEventArgs e)
        {
            AddContact addContact = new AddContact(frame, bt,contactsArray, user, mainFrame);
            frame.Navigate(addContact);

        }

        private void BTinvitations_Click(object sender, RoutedEventArgs e)
        {
            Invitations inv = new Invitations(user, frame, mainFrame, contactsArray, bt);
            frame.Navigate(inv);
        }
    }
}

