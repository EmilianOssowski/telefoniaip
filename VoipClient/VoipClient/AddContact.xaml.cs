﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VoipClient
{
    /// <summary>
    /// Interaction logic for AddContact.xaml
    /// </summary>
    public partial class AddContact : Page
    {
        SqlConnection m_dbConnection = new SqlConnection("Server=tcp:osadb.database.windows.net,1433;Initial Catalog=tip;Persist Security Info=False;User ID=benek;Password=Paradise6850;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        Frame frame;
        User user;
        List<User> contactsArray;
        Button bt;
        Frame mainFrame;
        public AddContact(Frame frame, Button bt, List<User> contactsArray ,User user, Frame mainFrame)
        {
            InitializeComponent();
            this.frame = frame;
            this.bt = bt;
            this.user = user;
            this.contactsArray = contactsArray;
            this.mainFrame = mainFrame;
        }

        private void BTsearchUserExit_Click(object sender, RoutedEventArgs e)
        {
            Contacts contacts = new Contacts(frame, bt, contactsArray, user,mainFrame);
            frame.Navigate(contacts);

        }



        private void BTaddToContacts_Click(object sender, RoutedEventArgs e)
        {
            User u = new User();
            try
            {
                m_dbConnection.Open();
                string sql = "SELECT username, mail, adress, status, idUser, idContactList  FROM TelIP_User u JOIN TelIP_ContactList c ON c.idOwner=u.idUser WHERE u.username = @username";
                SqlCommand command = new SqlCommand(sql, m_dbConnection);
                command.Parameters.Add(new SqlParameter("@username", TBsearchUser.Text));


                DataTable dt = new DataTable();
                SqlDataReader queryResult = command.ExecuteReader();
                dt.Load(queryResult);

                DataRow dw = dt.Rows[0];

                

                u.Username = dw[0].ToString();
                u.Mail = dw[1].ToString();
                u.Adress = dw[2].ToString();
                u.Status = Convert.ToBoolean(dw[3].ToString());
                u.IdUser = int.Parse(dw[4].ToString());
                u.IdContactList = int.Parse(dw[5].ToString());
                if (u.Username == user.Username) throw new Exception("Nie możesz dodać sam siebie");
                foreach (User c in contactsArray)
                {
                    if (c.Username == u.Username) throw new Exception("Kontakt jest już na liście");
                }

                //contactsArray.Add(u);

                ///Dodawanie znajomości do bazy
                string sqlAddContact = "INSERT INTO TelIP_Invitations(idHost, idGuest) values(@idHost, @idGuest)";
                SqlCommand commandAddContact = new SqlCommand(sqlAddContact, m_dbConnection);
                commandAddContact.Parameters.Add(new SqlParameter("@idHost", user.IdUser));
                commandAddContact.Parameters.Add(new SqlParameter("@idGuest", u.IdUser));

                commandAddContact.ExecuteNonQuery();

                ///Dodawanie znajomości do bazy
                //string sqlAddContact = "INSERT INTO TelIP_Match(idContactList, idUser) values(@idContactList, @idUser)";
                // SqlCommand commandAddContact = new SqlCommand(sqlAddContact, m_dbConnection);
                // commandAddContact.Parameters.Add(new SqlParameter("@idContactList", user.IdContactList));
                // commandAddContact.Parameters.Add(new SqlParameter("@idUser", u.IdUser));

                // commandAddContact.ExecuteNonQuery();

                ///Dodawanie kontrznajomości
                // string sqlAddContrContact = "INSERT INTO TelIP_Match(idContactList, idUser) values(@idContactList, @idUser)";
                // SqlCommand commandAddContrContact = new SqlCommand(sqlAddContrContact, m_dbConnection);
                // commandAddContrContact.Parameters.Add(new SqlParameter("@idContactList", u.IdContactList));
                // commandAddContrContact.Parameters.Add(new SqlParameter("@idUser", user.IdUser));

                // commandAddContrContact.ExecuteNonQuery();



                MessageBox.Show("Wysłano zaproszenie");

                Contacts contacts = new Contacts(frame, bt, contactsArray, user, mainFrame);
                frame.Navigate(contacts);

            }
            catch(InvalidOperationException)
            {
                MessageBox.Show("Nie znaleziono użytkownika", "Błąd");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            m_dbConnection.Close();


        }
    }
}
